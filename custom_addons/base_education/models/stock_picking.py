from odoo import api, fields, models, _


class StockPicking(models.Model):
    _inherit = "stock.picking"

    is_express_delivery = fields.Boolean(string="Express Delivery")

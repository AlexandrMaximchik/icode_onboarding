from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.constrains("order_line")
    def _check_exist_product_in_line(self):
        """
        Check that the 'sale.order' contains only unique units of 'product.template'
        """
        for order_id in self:
            exist_product_list = []
            for line_id in order_id.order_line:
                if line_id.product_id.id in exist_product_list:
                    raise ValidationError(_("Product should be one per line."))
                exist_product_list.append(line_id.product_id.id)

    @api.depends("order_line.product_id", "order_line")
    def _compute_product_template_id(self):
        """
        Links 'product.template' with 'sale.order' when adding a new line or changing a product in an existing line
        """
        for order_id in self:
            for line_id in order_id.order_line:
                selected_product_temp_ids = order_id.selected_product_temp_ids
                if line_id.product_template_id not in selected_product_temp_ids:
                    selected_product_temp_ids |= line_id.product_template_id
                    order_id.write(
                        {"selected_product_temp_ids": selected_product_temp_ids},
                    )

    selected_product_temp_ids = fields.Many2many(
        comodel_name="product.template",
        relation="sale_order_product_template_rel",
        column1="sale_order_id",
        column2="product_template_id",
        compute="_compute_product_template_id",
        store=True,
    )

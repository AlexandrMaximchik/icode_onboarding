from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.onchange("is_primary")
    def _onchange_is_primary(self):
        """
        When a partner is set as primary, all partners are set as not primary
        """
        for partner_id in self:
            contacts = partner_id.search([
                ("is_primary", "=", True),
                ("id", "=", partner_id.parent_id.child_ids.ids),
            ])
            contacts.update(
                {"is_primary": False,}
            )

    is_primary = fields.Boolean(
        string="Primary",
    )

    def unlink(self):
        """
        Overwrite
        When the only partner is primary, this partner is not deletable
        """
        for partner_id in self:
            check_lst = (
                len(partner_id.parent_id.child_ids.ids) != 1,
                not partner_id.is_primary,
            )
            if all(check_lst):
                super().unlink()

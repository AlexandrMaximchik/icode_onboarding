from odoo import api, fields, models, _


class StockMove(models.Model):
    _inherit = "stock.move"

    def _search_picking_for_assignation_domain(self):
        """
        Overrides
        Add a condition to the assignation domain
        """
        domain = super()._search_picking_for_assignation_domain()
        if isinstance(domain, list):
            domain.append(
                ("is_express_delivery", "=", self.sale_line_id.is_express_delivery)
            )
        return domain

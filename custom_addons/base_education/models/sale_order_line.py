from odoo import api, fields, models, _


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.onchange("product_id", "order_id.order_line")
    def _onchange_product_id(self):
        """
        Remove products already selected in the 'sale.order' from the drop-down menu of the 'product_template' field
        """
        domain = [
            ("id", "not in", self.order_id.selected_product_temp_ids.ids),
            ("sale_ok", "=", True),
            "|",
            ("company_id", "=", False),
            ("company_id", "=", self.order_id.company_id.id),
        ]
        return {"domain": {"product_template_id": domain}}

    is_express_delivery = fields.Boolean(string="Express Delivery")

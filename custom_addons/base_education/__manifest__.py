{
    "name": "Base Educational Module",
    "summary": "Base Educational Module",
    "sequence": 10,
    "description": "",
    "depends": [
        "base",
        "sale",
        "stock",
    ],
    "data": [
        "views/res_partner_view.xml",
        "views/sale_order_view.xml",
    ],
    "installable": True,
    "assets": {},
}
